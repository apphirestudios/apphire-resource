var Apphire = require('apphire');

require('coffee-script').register()
global.app = new Apphire()

require('./resource')
require('./model')
require('./mixins/schema')
require('./flush')

app.start()

