Model = require '../../lib/model'
SchemaMixin = require '../../lib/mixins/schema'

class MyModel extends Model.with SchemaMixin
MyModel.setTransport app

acc1Id =  "000000000000000000000001"
acc2Id =  "000000000000000000000002"
user1Id = "000000000000000000000010"
user2Id = "000000000000000000000020"
user2Id = "000000000000000000000030"

if app.isServer
  MyModel.connect('localhost/test1')

extend = (what, byWhat)->
  return if not (what? and byWhat?)
  for k,v of byWhat
    what[k] = v

delay = (interval, fn)->
  setTimeout fn, interval

class Root extends MyModel
  constructor: (initial)-> super(initial)
  preloaded: ['getAccount']

  getAccount:
    client: ->
      return @storage.Account[0]

    server: ->
      return (yield @db.Account.find(id: acc1Id)).map (rec)-> new Account(rec)

class Account extends MyModel
  constructor: (initial)-> super(initial)
  synced: ['addUser', 'deleteUser']
  preloaded: ['getMyUsers', 'getAllUsers']
  immediate: ['getUserCount']

  schema:
    createdOn: {type: Date, required: false, default: new Date()}

  load: (id)->
    yield Account.db.Account.findOne({id: id})

  getMyUsers:
    client: ->
      @storage.User.filter (user)=>
        user.accountId is @
    server: ->
      return users = (yield @db.User.find({accountId: @id})).map (rec)-> new User(rec)

  getUserCount: ->
    return yield @db.User.count()

  addUser: (data)->
    data.accountId = @id
    user = new User(data)
    @storage.User.push user
    return user

  deleteUser: (user)->
    @storage.User.delete user

  getAllUsers:
    client: ->
      return @storage.User

    server: ->
      return users = (yield @db.User.find()).map (rec)-> new User(rec)


class User extends MyModel
  constructor: (initial)-> super(initial)
  preloaded: ['getAccount']
  schema:
    accountId: {type: String, required: false, ref: 'Account'}
    createdOn: {type: Date, required: false, default: new Date()}
    name: {type: String, required: false}
    email: {type: String, unique: true, required: false,  validate: -> true }
    sub: {
      one: {type: String, required: false}
    }
    #password: {type: String, minlength: [7, 'Пароль слишком короткий'], required: true}

  getAccount:
    client: ->
      @storage.Account.filter (acc)=>
        acc.id is @accountId

    server: ->
      new Account yield @db.Account.findOne(id: @accountId)

  insert: ()->
    yield @db.User.insert(@.state)

  select: (id)->
    yield User.db.User.findOne({id: id})

  update: ()->
    yield @db.User.findOneAndUpdate {id: @id}, @.state

  delete: ->
    yield @db.User.findOneAndDelete {id: @id}

  isomorphic:
    immediate:
      getMe: ->
        return @


MyModel.register(Root)
MyModel.register(Account)
MyModel.register(User)

root = undefined
if app.isClient
  module.exports =
    add: ->
      describe "load", ->
        t = undefined
        beforeEach ->
          yield app.request.post '/flush'
          root = new Root({id: 0})
          root.flushStorage()
          yield root.loadLinkedObjects()

        describe 'init', ->
          it 'get 1 level', ->
            acc = root.getAccount()
            acc.id.should.equal acc1Id

          it 'get 2 level', ->
            acc = root.getAccount()
            users = acc.getMyUsers()
            users[0].accountId.should.equal acc
            all = acc.getAllUsers()
            all.length.should.equal 3

          it 'set', ->
            acc = root.getAccount()
            user = acc.getMyUsers()[0]
            user.name = "Updated"
            u = yield user.getMe()

            u.name.should.equal user.name

          it 'set nested', ->
            acc = root.getAccount()
            user = acc.getMyUsers()[0]
            log user
            user.sub.one = "Updated"

            u = yield user.getMe()
            u.sub.one.should.equal user.sub.one


          it 'add', ->
            acc = root.getAccount()
            (yield acc.getUserCount()).should.equal 3
            user = acc.addUser({name: 'Added'})
            id = user.id
            u = yield user.getMe()
            u.name.should.equal 'Added'
            u.id.should.equal id
            (yield acc.getUserCount()).should.equal 4

          it 'delete', ->
            acc = root.getAccount()
            user = acc.getMyUsers()[0]
            acc.deleteUser user
            (yield acc.getUserCount()).should.equal 2
            acc.storage.User.length.should.equal 2
            user = acc.getMyUsers()[0]
            acc.deleteUser user
            (yield acc.getUserCount()).should.equal 1
            user = acc.getMyUsers()[0]
            expect(user).to.be.undefined




