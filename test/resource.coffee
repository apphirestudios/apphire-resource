{ResourceManager, Resource} = require '../lib/resource'
ResourceManager.setTransport app

delay = (interval, fn)->
  setTimeout fn, interval


class One extends Resource
  state: 1
  constructor: ->
    super()

  saveState: (state)->
    ResourceManager.backendState = state

  loadState: (id)->
    return ResourceManager.backendState

  isomorphic:
    immediate:
      echoSync: (val)->
        return val
      echoAsync: (val)->
        yield new Promise delay.bind(@, 10)
        return val
      setState: (val)->
        ResourceManager.state = val
      getState: ()->
        return ResourceManager.state
      getBackendState: ->
        return ResourceManager.backendState
      getCtx: ()->
        return @ctx
    synced:
      inc: ->
        ResourceManager.state += 1
      update: (state)->
        @state = state




if app.isClient
  module.exports =
    add: ->
      describe "init", ->
        it 'with no isomorphic', ->
          class T extends Resource
            constructor: -> super()
          return new T()

        it 'inits ok', ->
          class T extends Resource
            isomorphic:
              synced:
                t: ->
              immediate:
                t2: ->
            constructor: -> super()
          t = new T()
          t.t.should.be.function
          t.t2.should.be.function
          expect(t.isomorphic).to.be.undefined


        it 'throws when defined as synced but not exists', ->
          class T extends Resource
            synced: ['some']
            constructor: -> super()
          (-> new T()).should.throw /defined as synced/

        it 'throws when defined as immediate but not exists', ->
          class T extends Resource
            immediate: ['some']
            constructor: -> super()
          (-> new T()).should.throw /defined as immediate/


      describe "immediate", ->
        t = undefined
        beforeEach ->
          t = new One()
        it 'echo - sync', ->
          resp = yield t.echoSync(1)
          resp.should.equal 1
        it 'echo - async', ->
          resp = yield t.echoAsync(1)
          resp.should.equal 1

        it 'ctx', ->
          resp = yield t.getCtx(1)
          resp.request.should.be.object
          resp.response.should.be.object

      describe "synced", ->
        t = undefined
        beforeEach ->
          t = new One()
          ResourceManager.state = 0
          yield t.setState 0
        it 'one', ->
          (yield t.getState()).should.equal 0
          t.inc()
          ResourceManager.state.should.equal 1
          (yield t.getState()).should.equal 1

        it 'two in a row', ->
          (yield t.getState()).should.equal 0
          t.inc()
          t.inc()
          ResourceManager.state.should.equal 2
          (yield t.getState()).should.equal 2

      describe "load and save", ->
        it 'persist state', ->
          t = new One(id: 'one')
          backendState = yield t.getBackendState()
          backendState.should.equal 1
          t.update(2)
          backendState = yield t.getBackendState()
          backendState.should.equal 2

else
  ResourceManager.state = 0
  ResourceManager.register One
  ResourceManager.backendState = 1