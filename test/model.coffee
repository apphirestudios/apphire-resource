Model = require '../lib/model'
SchemaMixin = require '../lib/mixins/schema'

class MyModel extends Model
MyModel.setTransport app

sub1Id =  "000000000000000000000001"
sec1Id = "000000000000000000000010"
sec2Id = "000000000000000000000020"

if app.isServer
  MyModel.connect('localhost/test1')

extend = (what, byWhat)->
  return if not (what? and byWhat?)
  for k,v of byWhat
    what[k] = v

delay = (interval, fn)->
  setTimeout fn, interval

class Base extends MyModel
  constructor: (initial)-> super(initial)
  preloaded: ['getSub']

  getSub:
    client: ->
      return @storage.Sub[0]

    server: ->
      return (yield @db.Sub.find(id: sub1Id)).map (rec)-> new Sub(rec)


MyModel.register(Base)

class Sub extends MyModel
  constructor: (initial)-> super(initial)
  preloaded: ['getMySecs', 'getAllSecs']

  prepare: -> async =>
    extend @, yield @db.Sub.findOne({id: @state.id})

  getMySecs:
    client: ->
      @storage.Sec.filter (user)=>
        user.state.subId is @.state.id
    server: ->
      (yield @db.Sec.find({subId: @state.id})).map (rec)-> new Sec(rec)
  getAllSecs:
    client: ->
      return @storage.Sec

    server: ->
      return users = (yield @db.Sec.find()).map (rec)-> new Sec(rec)

MyModel.register(Sub)

class Sec extends MyModel
  constructor: (initial)-> super(initial)

  prepare: -> async =>
    extend @,  yield @db.Sec.findOne({id: @state.id})

  isomorphic:
    immediate:
      getMe: ->
        return @

MyModel.register(Sec)

root = undefined
if app.isClient
  module.exports =
    add: ->
      describe "load", ->
        t = undefined
        beforeEach ->
          yield app.request.post '/flush'
          root = new Base()
          root.flushStorage()
          yield root.loadLinkedObjects()

        describe 'init', ->
          it 'get 1 level', ->
            s = root.getSub()
            s.constructor.should.equal Sub

          it 'get 2 level', ->
            s = root.getSub()
            secs = s.getMySecs()
            secs[0].state.subId.should.equal s.state.id
            all = s.getAllSecs()
            all.length.should.equal 2


