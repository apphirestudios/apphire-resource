mongo = require '../lib/mongo'
db = mongo('localhost/test1')

cors = require('koa-cors')
corsError = require 'koa-cors-error'


sub1Id =  "000000000000000000000001"
sec1Id =  "000000000000000000000010"
sec2Id = "000000000000000000000020"

acc1Id =  "000000000000000000000001"
acc2Id =  "000000000000000000000002"
user1Id = "000000000000000000000010"
user2Id = "000000000000000000000020"
user3Id = "000000000000000000000030"

app.use cors()
app.use corsError
app.router.post '/flush', (next)->
  Sub = db.get('Sub')
  yield  Sub.remove()
  Sec = db.get('Sec')
  yield Sec.remove()
  yield Sub.insert(id: sub1Id)
  yield Sec.insert(id: sec1Id, subId: sub1Id)
  yield Sec.insert(id: sec2Id, subId: sub1Id)


  User = db.get('User')
  yield  User.remove()
  Account = db.get('Account')
  yield Account.remove()
  yield Account.insert(id: acc1Id)
  yield Account.insert(id: acc2Id)

  yield User.insert(accountId: acc1Id, name: 'TestUser', id: user1Id)
  yield User.insert(accountId: acc1Id, name: 'TestUser2', id: user2Id)
  yield User.insert(accountId: acc2Id, name: 'TestUser3', id: user3Id)

  @body = {}
  yield next

