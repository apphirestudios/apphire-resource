async = require 'co'

{forOwn, hasOwn, extend, isFunction, isArray, isPlainObject} = require 'apphire-helpers'

quenuePromise = null
quenue = []

sendQuenue = -> async ->
  if quenuePromise then return quenuePromise
  quenuePromise = new Promise (resolve)-> async ->
    while quenue.length > 0
      try
        response = yield ResourceManager.transport.request.post "/resource/quenue", {quenue}
        quenue.splice 0, response.processed
      catch e
        yield new Promise (r)-> setTimeout r, 10000
    resolve()

  yield quenuePromise
  quenuePromise = null

quenueDigestLoop = setInterval sendQuenue, 10
registered = {}


sendSingle = (transaction)-> async ->
  return yield ResourceManager.transport.request.post "/resource/#{transaction.resourceName}/#{transaction.id}/#{transaction.methodName}", transaction

getResource = (resourceName)->
  if not registered[resourceName]
    throw new Error "Resource name #{resourceName} is not registered"
  return registered[resourceName]

processTransaction = (transaction, ctx)-> async ->
  RegisteredResourceClass = getResource(transaction.resourceName)

  state = {}
  if RegisteredResourceClass::loadState?
    state = yield async -> RegisteredResourceClass::loadState(transaction.id)

  instance = new RegisteredResourceClass(state)
  instance.ctx = ctx

  method = instance[transaction.methodName]

  transaction.methodArgs = objectify(transaction.methodArgs)

  if not method
    throw new Error "Method name #{transaction.methodName} is not defined in #{transaction.resourceName}"
  returned = yield async -> method.apply instance, transaction.methodArgs

  if RegisteredResourceClass::saveState?
    yield async -> RegisteredResourceClass::saveState.bind(instance)(instance.state)

  return returned

convertDotToNested = (obj)->
  for k, v of obj
    attrParts = k.split('.')
    if attrParts.length > 1 #then it's 'attribute.with.nested.fields'
      node = obj
      for part, index in attrParts when index < attrParts.length - 1
        node = node[part] ?= {}
      node[attrParts[attrParts.length - 1]] = v

ResourceManager =
  setTransport: (t)->
    return if ResourceManager.transport?
    ResourceManager.transport = t
    if ResourceManager.transport.isServer
      ResourceManager.transport.router.post "/resource/:resourceName/:id/:methodName", (next)->
        @body =
          returned: yield processTransaction(@request.body, @)
        yield next

      ResourceManager.transport.router.post "/resource/quenue", (next)->
        @body = {returned: []}
        for transaction in @request.body.quenue
          @body.returned.push yield processTransaction(transaction, @)
        @body.processed = @body.returned.length
        yield next

  register: (ResourceClasses)->
    if ResourceClasses.constructor isnt Array
      ResourceClasses = [ResourceClasses]
    for ResourceClass in ResourceClasses
      ResourceClass::schema.id = {type: String, required: true}
      registered[ResourceClass.name] = ResourceClass


throwDefinitionError = (obj, msg)->
  throw new Error "Definition error in resource '#{obj.constructor.name}'. #{msg}"



objectify = (data)->
  if m = data?.__modelName__ #TODO - recursively
    Model = getResource(m)
    #delete data.__modelName__
    return new Model(data)
  else
    if isPlainObject(data)
      for k, v of data
        data[k] = objectify(v)
    else if isArray(data)
      for elem, index in data
        data[index] = objectify(elem)
    return data

class Resource
  state: {}
  resourceName: undefined

  constructor: ()->
    if @immediate?
      for methodName in @immediate
        if not @[methodName] then throwDefinitionError @, "Method name '#{methodName}' was defined as immediate but is not exist"
        @isomorphic ?= {}
        @isomorphic.immediate ?= {}
        @isomorphic.immediate[methodName] = @[methodName]
    if @synced?
      for methodName in @synced
        if not @[methodName] then throwDefinitionError @, "Method name '#{methodName}' was defined as synced but is not exist"
        @isomorphic ?= {}
        @isomorphic.synced ?= {}
        @isomorphic.synced[methodName] = @[methodName]


    forOwn @isomorphic?.immediate, (methodName, methodDef)=>
      #if @[methodName]? then throw new Error "Method name #{methodName} is defined more than once."
      if ResourceManager.transport.isClient
        @[methodName] = (methodArgs...)=> async =>
          transaction =
            resourceName: @.constructor.name
            methodName: methodName
            timestamp: new Date()
            id: @state.id
            methodArgs: methodArgs
          yield sendQuenue()
          body = yield sendSingle transaction
          returned = objectify(body.returned)
          returned

      else
        @[methodName] = methodDef.bind @

    forOwn @isomorphic?.synced, (methodName, methodDef)=>
      #if @[methodName]? then throw new Error "Method name #{methodName} is defined more than once."
      #if not isFunction(methodDef) then throwDefinitionError @,  "Synced method '#{methodName}' should be a function"
      if ResourceManager.transport.isClient
        @[methodName] = (methodArgs...)=>
          transaction =
            resourceName: @.constructor.name
            methodName: methodName
            timestamp: new Date()
            id: @state.id
            methodArgs: methodArgs
          if methodDef.client then methodDef = methodDef.client.bind @
          resultFromClient =  methodDef.apply @, methodArgs
          quenue.push transaction #TODO (very important) - make complete clone of all arguments since they can be changed during function
          return resultFromClient
      else
        if methodDef.server then methodDef = methodDef.server.bind @
        @[methodName] = methodDef.bind @
    @isomorphic = undefined

module.exports = {Resource, ResourceManager}