{ResourceManager} = require '../resource'
Proxy = require 'apphire-proxy-object'
{forOwn, hasOwn, extend, isArray, isPlainObject, isObject, isFunction} = require 'apphire-helpers'

defGetterSetter = (obj, name, {get, set})->
  Object.defineProperty obj, name,
    configurable: true
    enumerable: true
    get: get
    set: set or ->

generateObjectId = ->
  timestamp = ((new Date).getTime() / 1000 | 0).toString(16)
  timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, ->
    (Math.random() * 16 | 0).toString 16
  ).toLowerCase()

getAttrSchema = (schema, path, obj)->
  current = schema
  for p in path.split('.')
    if not isNaN Number(p) then p = '0'
    if not current[p]? then throwValidationError obj, "Cannot set attribute '#{path}'. It not exist in schema."
    current = current[p]
  return current

throwNotLoadedError = (obj, msg)->
  throw new Error "Load error in model '#{obj.modelName}', id: #{obj.id}. #{msg}."

throwValidationError = (obj, msg)->
  throw new Error "Validation error in model '#{obj.modelName}', id: #{obj.id}. #{msg}"

throwDefinitionError = (obj, msg)->
  throw new Error "Definition error in model '#{obj.modelName}', id: #{obj.id}. #{msg}"

validate = (val, schema, path, obj)->
  path ?= ''
  if isPlainObject(schema) and not schema.type?  #TODO - make more advanced leaf checking
    if val?
      if not isPlainObject(val) then throwValidationError obj, "Attribute '#{path}' should be object"
      for k, v of schema
        validate val[k], v, "#{path}.#{k}", obj
      for k, v of val
        if not schema[k]? then throwValidationError obj, "Cannot set attribute '#{path}.#{k}'. It not exist in schema."

  else if isArray(schema)
    if val?
      if not isArray(val) then throwValidationError obj, "Attribute '#{path}' should be array"
      for e, index in val
        validate e, schema[0], "#{path}.#{index}", obj
  else
    if schema.required and not val?
      throwValidationError obj, "Attribute '#{path}' is required by schema."

    if schema.validator then schema.validator(val)

createDefaultFromSchema = (schema)->
  if schema.type?
    if isFunction(schema.default)
      return schema.default()
    return schema.default or undefined #TODO - make more advanced leaf checking
  ret = {}
  if isArray schema
    ret = []
  else
    for k, v of schema
      ret[k] = createDefaultFromSchema(schema[k])
  ret


setAttr = (value, path, schema, obj)->
  curSchema = getAttrSchema(schema, path, obj)
  ret = createDefaultFromSchema(curSchema)
  if isPlainObject(value)
    for k, v of value
      ret[k] = setAttr v, (path + '.' + k), schema, obj
  
  else if isArray(value)
    for el, index in value
      ret.push setAttr(el, (path + '.' + index), schema, obj)

  else
    ret = value
    if curSchema.ref? and value?#If it's reference then set id instead of object
      if value.state?.id?
        if not (value.modelName is curSchema.ref)
          throwNotLoadedError obj, "Cannot set referenced attribute #{path}. Expected to get instance of '#{curSchema.ref}'. Instead got instance of #{value.modelName}."
        ret = value.state.id
  return ret


addReferencedAttribute = (modelName, attrName)-> #TODO Handle multiple refs to same model properly
  virtualAttrName = modelName.charAt(0).toLowerCase() + modelName.substr(1) + "s"
  if @schema? and @schema[virtualAttrName]
    throwDefinitionError @, "Attribute name #{virtualAttrName} is not allowed because we have virtual attribute with same name. Please choose another name."
  defGetterSetter @, virtualAttrName,
    get: ->
      if ResourceManager.transport.isServer then throw new Error "Virtual methods cannot be accessed on server for now, sorry"
      return @storage[modelName].filter (elem)=> (elem.state[attrName] is @state.id)
    set: -> throw new Error "Attribute #{attrName} is virtual and cannot be set"

augmentWithReferencedResources = ->
  for modelName, ModelDef of @models
    for attrName, schema of ModelDef::schema
      if schema.ref is @modelName then addReferencedAttribute.call @, modelName, attrName

module.exports = class SchemaMixin
  initialize: (initialState)->
    modelName = initialState.__modelName__
    delete initialState.__modelName__
    if not @schema then throw new Error "No schema defined in #{@modelName}"
    initialState.id ?= generateObjectId()
   
    @synced ?= []
    @synced = @synced.concat ['__setState', '__push', '__remove']
    
    @__setState = (val, path)-> #this will be executed only serverside
      if ResourceManager.transport.isServer
        cur = @
        parts = path.split('.')
        last = parts.pop()
        for p in parts
          cur = cur[p]

        cur[last] = val

    @set = (val, path)->
      newValue = setAttr(val, path, @schema, @)
      curSchema = getAttrSchema(@schema, path, @)
      validate(newValue, curSchema, path, @)
      if ResourceManager.transport.isClient #let's send this update to server
        @__setState(val, path)
      return newValue


    @get = (val, path)->
      schema = getAttrSchema @schema, path, @
      if schema.ref? and ResourceManager.transport.isClient #If it's reference then return object instead of it
        orig = val
        if not orig? then return null #If no id is present, well, we have to return null
        val = @storage[schema.ref].filter((el)-> el.id is val)[0]
        if not val? then return null
        
        #throwNotLoadedError @, "Cannot get attribute #{path}. Reference object id: '#{orig}' from '#{schema.ref}' collection is not loaded"
      return val
    
    syncedConfig =
      setTransformers: [ (newVal, oldVal, path)=> return @set newVal, path ]
      getTransformers: [ (val, path)=>  return @get val, path ]
    
    @state = createDefaultFromSchema(@schema)
    for k, v of initialState
      @state[k] = setAttr(v, k, @schema, @)
    
    validate(@state, @schema, undefined, @) 
    augmentWithReferencedResources.call(@)

    Proxy.extend @, @state, syncedConfig
    @state.__modelName__ = modelName

    @add = ()->
      @__push(@)

    @__push =
      client: (rec)->
        @storage.User.push rec

      server: (rec)->
        yield @insert(rec)

    @__remove =
      client: ->
        #@storage.User.push user
      server: ->
        yield @remove()


