{ResourceManager, Resource} = require './resource'
helpers = require 'apphire-helpers'

guid = ->
  s4 = ->
    Math.floor((1 + Math.random()) * 0x10000).toString(16).substring 1
  s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()


checkPrerequisites = ()->
  if @modelName is 'Model'
    throw new Error "Model is abstract class. You should inherit from it."
  #if not @prepare
  #  throw new Error "You should define 'prepare' method for #{@modelName} class."


processInstance = (instance, loaded)-> async ->
  loaded[instance.modelName] ?= {}
  loaded[instance.modelName][instance.state.id] = instance
  preloadedMethods = instance.preloaded
  if preloadedMethods
    for methodName in preloadedMethods
      result = yield instance[methodName]()
      if result?
        if result.constructor isnt Array then result = [result]
        for element in result
          if not (element instanceof Model)
            throw new Error "Instance '#{instance.state.id}' of '#{instance.modelName}' method '#{methodName}' expected to return model instances, instead got #{JSON.stringify(element)}"
          if not loaded[element.modelName]?[element.id]? then yield processInstance(element, loaded)


throwDefinitionError = (obj, msg)->
  throw new Error "Definition error in model '#{obj.modelName}'. #{msg}"

class Model extends Resource
  @with: (mixins...) ->
    class Mixed extends Model
    for mixin in mixins by -1 #earlier mixins override later ones
      for name, method of mixin::
        Mixed::[name] = method
    return Mixed

  @connect: (connString)->
    eval 'var mongo = require("./mongo")'
    Model.dbConn = mongo(connString)
  @setTransport: (transport)->
    ResourceManager.setTransport(transport)
  @storage: {}
  @models: {}
  @db: {}
  @register: (ModelClass)->
    Model.models[ModelClass.name] = ModelClass
    if ResourceManager.transport.isServer
      Model.db[ModelClass.name] = Model.dbConn.get(ModelClass.name)
    else
      Model.storage[ModelClass.name] = s = []
      s.delete = (elem)->
        for el, i in s
          if elem.id is el.id then index = i
        if index? then s.splice(index, 1) else throw new Error "Element with id #{elem.id} not found in '#{ModelClass.name}'' collection"

    ResourceManager.register(ModelClass)

  flushStorage: ->
    for k, v of Model.storage
      v.splice(0, v.length)

  loadLinkedObjects: ->
    rec = yield @__getMeAndMyChildren()
    for modelName, instances of rec
      @storage[modelName] ?= []
      ModelClass = @models[modelName]
      for instanceId, instanceState of instances
        @storage[modelName].push instanceState

  loadState: (id)-> async =>
    if @select then yield @select(id)

  saveState: (state)-> async =>
    if @update then yield @update(state)
    for operation in @__ops
      switch operation.type
        when 'insert'
          yield operation.item.insert()
        when 'delete'
          yield operation.item.delete()

  constructor: (@state = {})->
    if not @modelName then throw new Error "Please set 'modelName' property to model class"
    if ResourceManager.transport.isClient
      @storage = Model.storage
    else
      @db = Model.db
      @storage = {}
      @__ops = []
      helpers.forOwn Model.models, (modelName)=>
        @storage[modelName] = [] #let's mock storage serverside for isomorfic push method available
        @storage[modelName].push = (item)=> #TODO - find a better way for this
          @__ops.push
            type: 'insert'
            item: item
        @storage[modelName].delete = (item)=>
          @__ops.push
            type: 'delete'
            item: item


    @models = Model.models

    @immediate ?= []
    @immediate.push '__getMeAndMyChildren'
    @__getMeAndMyChildren = ->
      loaded = {}
      yield processInstance(@, loaded)
      return loaded

    if @preloaded
      for p in @preloaded
        if not @[p]? then throwDefinitionError @, "Method name #{p} is defined as preloaded but is not exist."
        if ResourceManager.transport.isClient
          if not @[p].client? then throwDefinitionError @, "Preloaded method name #{p} should have 'client' attribute."
          @[p] = @[p].client
        else
          if not @[p].server? then throwDefinitionError @, "Preloaded method name #{p} should have 'server' attribute."
          @isomorphic ?= {}
          @isomorphic.immediate ?= {}
          @isomorphic.immediate[p] = @[p].server
          delete @[p]
          #@immediate = @immediate.concat @preloaded

    if @initialize? then @initialize(@state)
    super()
    delete @isomorphic

    checkPrerequisites.call @
  toJSON: ()->
    ret = @state
    ret.__modelName__ = @modelName
    return ret

module.exports = Model